class MinimalCode {
    public static void main(String[] args) {
        System.out.println(new Simple().test1());
    }
}

class Simple {
    
  public int test1() {
    boolean x;
    boolean y;
    x = true;
    y = false;

    if (x >= y) {
      System.out.println(1);
    } else {
      System.out.println(0);
    }
    return 0;
  }

}


