class MinimalCode {
    public static void main(String[] args) {
        System.out.println(new Simple().f());
    }
}

class Simple {
    public int f() {
	int b;
	b = 1;
	if (3 >= 2) {
	    b = 3;
	}
	else {
	    b = 2;
	}
        return b;
    }
}

