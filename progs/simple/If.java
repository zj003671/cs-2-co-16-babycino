class MinimalCode {
    public static void main(String[] args) {
	 System.out.println(new Simple().f());
    }
}

class Simple {
    public int f(){
	 int b;
	 if (1)
	     b = 2;
	 else
	     b = 3;
	 return b;
    }
}
